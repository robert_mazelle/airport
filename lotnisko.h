#pragma once
#include <iostream>
#include "samolot.h"
#include "kontroler.h"
#include "hangar.h"

using namespace std;

class CLotnisko
{
    CSamolot* ladujace[100];
    CSamolot* czekajace[100];
    CSamolot* garazowane[100];
    CKontroler* andrzej;
    CHangar* hangar_A;
    CHangar* hangar_B;
    CHangar* hangar_C;
    int pasazerowie;
    void PrzekierujNaInneLotnisko(CSamolot* Przekierowany);
    int Wyprowadz(CSamolot* Wyprowadzany);
    void DajPasazerow(CSamolot* Pasazerski);
    void Garazuj(CSamolot* Garazowany);

public:
    CLotnisko(CKontroler* Kontroler);
    ~CLotnisko();
    void NadlatujacySamolot();
    void NadlatujacySamolot(float Rozpietosc,char* Typ,int LiczPasazerow);
    void NowySamolot();
    void NowySamolot(float Rozpietosc,char* Typ,int LiczPasazerow);
    void Ladowanie();
    void Startowanie();
    void NowyCykl();
    int Zlicz(char Ktore);
    void LosujStartujacy(int IleSamolotow);
    void NowiPasazerowie();

};
