#include <iostream>
#include "samolot.h"
#include "kontroler.h"
#include "lotnisko.h"
#include "symulacja.h"
#include "hangar.h"

using namespace std;

int main()
{
    CKontroler kontrol;
    CLotnisko chopina(&kontrol);
    CSymulacja symulacja(&chopina);
    symulacja.Symuluj();
    new CSamolot(10,"samolot",100);



    return 0;
}
