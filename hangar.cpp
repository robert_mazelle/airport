#include "hangar.h"
#include <ctime>
#include <cstdlib>
#include "time.h"


using namespace std;

CHangar::CHangar(int miejsca)
{
    l_miejsc=miejsca;
    for(int i=1;i<l_miejsc+1;i++)
    {
        miejsce[i]=0;
    }
}

CHangar::~CHangar()
{

}

void CHangar::WprowadzDoHangaru(CSamolot* samolot)
{
    for(int i=1;i<l_miejsc+1;i++)
    {
        if(miejsce[i]==0)
        {
            samolot->DajMiejsce(i);
            miejsce[i]=1;
            cout<<"Samolot zostaje wprowadzony do hangaru "<<samolot->ZwrocHangar()<<endl;
            break;
        }
    }
}

int CHangar::WyprowadzZHangaru(CSamolot* samolot)
{
    if(SprawdzStan(samolot)==0)
    {
    miejsce[samolot->ZwrocMiejsce()]=0;
    samolot->DajHangar(NULL);
    cout<<"Samolot zostaje wyprowadzony z hangaru"<<endl;
    return 0;
    }
    else return 1;
}

int CHangar::SprawdzStan(CSamolot* samolot)
{
    float sprawnosc;
    int wiek=samolot->ZwrocWiek();
    cout<<"Wiek samolotu: "<<wiek;
    if(wiek==1) cout<<" rok"<<endl;
    else if(5>wiek>1) cout<<" lata"<<endl;
    else cout<<" lat"<<endl;
    if(wiek==0) sprawnosc=100;
    else sprawnosc=100-rand()%(2*wiek);
    cout<<"Sprawnosc samolotu wynosi: "<<sprawnosc<<endl;
    if (sprawnosc<75)
    {
        cout<<"Samolot nie nadaje sie do lotu, zostaje oddany do naprawy"<<endl;
        return 1;
    }
    else return 0;
}

int CHangar::SprawdzMiejsca()
{
    int licznik=0;
    for(int i=0;i<l_miejsc;i++)
    {
        if(miejsce[i]==1) licznik++;
    }
    licznik=l_miejsc-licznik;
    return licznik;
}

void CHangar::Informacja()
{
    cout<<"Hangar ma pojemnosc: "<<l_miejsc<<endl;
}
