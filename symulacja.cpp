#include "lotnisko.h"
#include "symulacja.h"
#include <ctime>
#include <cstdlib>
#include "time.h"
#include <windows.h>
#include "conio.h"
#include "stdlib.h"
#include <ios>


CSymulacja::CSymulacja(CLotnisko* lot)
{
    lotnisko=lot;
}

CSymulacja::~CSymulacja()
{

}

void CSymulacja::Symuluj()
{
    int zliczenie,akcja,licznik3=0;
    cout<<endl<<"Aby dodac:"<<endl<<"  -zakonczyc program, wcisnij ESC"<<endl<<"  -nowy losowy samolot do garazu, wcisnij strzalke w gore,"<<endl<<"  -nowy losowy samolot na horyzoncie, wcisnij strzalke w prawo"<<endl<<"  -nowy samolot i okreslic jego parametry, wcisnij strzalke w dol"<<endl<<endl<<"///////////////////////////////////////"<<endl<<endl;
    while(1)
    {
        Sprawdz();

    lotnisko->NowyCykl();
    lotnisko->NowiPasazerowie();
    if(licznik2%2==0)
    {
    akcja=rand()%2;
    if(akcja==0) lotnisko->NowySamolot();
    else lotnisko->NadlatujacySamolot();
    cout<<endl<<"**************************"<<endl;
    Sleep(3000);
    }
    Sprawdz();
    akcja=rand()%2;
    if(akcja==0)
    {
        startowanie:
        zliczenie=lotnisko->Zlicz('g');
        if(zliczenie<1)
        {
            licznik3++;
            if(licznik<3) goto ladowanie;
            else goto koniec;
        }
        else
        {
            lotnisko->LosujStartujacy(zliczenie);
            Sleep(1000);
            cout<<endl;
            lotnisko->Startowanie();

    }

    }
    else
    {
        ladowanie:
        zliczenie=lotnisko->Zlicz('l');
        if(zliczenie<1)
            {
                licznik3++;
                 if(licznik<3) goto startowanie;
                 else goto koniec;
            }
        else lotnisko->Ladowanie();
    }
    koniec:

    licznik++;
    licznik2++;
    licznik3=0;
    Sprawdz();
    Sleep(3000);

    }
}

void CSymulacja::Sprawdz()
{
     if(kbhit())
    {
        int znak=getch();
        if(znak==27)
        {
            cout<<endl<<"Program zakonczony przez uzytkownika"<<endl;
            exit(0);
        }
        if(znak==72)
        {
            cout<<"Samolot zostal dodany przez uzytkownika"<<endl;
            lotnisko->NowySamolot();
            cout<<endl<<"**************************"<<endl;
            Sleep(4000);
        }
        if(znak==77)
        {
            cout<<"Samolot zostal dodany przez uzytkownika"<<endl;
            lotnisko->NadlatujacySamolot();
            cout<<endl<<"**************************"<<endl;
            Sleep(4000);
        }
        if(znak==80)
        {
            char* typ=new char;
            char* rozpietosc=new char;
            char* l_pasazerow=new char;
            int licznikk=0,zmienna=0;
            char wybor;
            do
            {
            cout<<"Wpisz nazwe samolotu: ";
            try
            {
                cin.getline(typ,20);
                for (int i=0;i<20;i++) if(typ[i]==32) throw 1;
                zmienna++;
            }
             catch(int nr)
              {
                  cerr << "BLAD nr "<< nr;
                  if(nr==1) cout<<", zostala wpisana spacja, sprobuj ponownie uzywajac '_'"<<endl;
                  zmienna=0;
              }
               catch(...)
              {
                  cerr << "Wystapil nieokreslony blad"<<endl;
              }
            }while(zmienna==0);


            do
                {
            cout<<"Wpisz rozpietosc: ";
              try
              {
                  cin.getline(rozpietosc,20);
                  if(!JestLiczba(rozpietosc)) throw 1;
                  if(atof(rozpietosc)==0) throw 2;
                  for (int i=0;i<20;i++) if(rozpietosc[i]==32) throw 3;
                  zmienna++;
              }
              catch(int nr)
              {
                  cerr << "BLAD nr "<< nr;
                  if(nr==1) cout<<", nie zostala wpisana liczba"<<endl;
                  if(nr==2) cout<<", rozpietosc nie moze byc rowna 0"<<endl;
                  if(nr==3) cout<<", zostala wpisana spacja"<<endl;
                  zmienna=0;
                  }
              catch(...)
              {
                  cerr << "Wystapil nieokreslony blad"<<endl;
                  }

                }while(zmienna==0);

            do
                {
              cout<<"Wpisz maksymalna liczbe pasazerow: ";
              try
              {
                  cin.getline(l_pasazerow,20);
                  if(!JestLiczba(l_pasazerow)) throw 1;
                  for (int i=0;i<20;i++) if(l_pasazerow[i]==32) throw 2;
                  zmienna++;
              }
              catch(int nr)
              {
                  cerr << "BLAD nr "<< nr;
                  if(nr==1) cout<<", nie zostala wpisana liczba"<<endl;
                  if(nr==2) cout<<", zostala wpisana spacja"<<endl;
                  zmienna=0;
                  }
              catch(...)
              {
                  cerr << "Wystapil nieokreslony blad"<<endl;
                  }

                }while(zmienna==0);

            do
                {
            cout<<"Samolot ma byc dodany do garazu (g) czy ma sie pojawic na horyzoncie (h)? ";
            cin>>wybor;
            if(wybor=='g')
            {
                cout<<endl<<"Samolot zostal dodany przez uzytkownika"<<endl;
                lotnisko->NowySamolot(atof(rozpietosc),typ,atoi(l_pasazerow));
                licznikk++;
                cout<<endl<<"**************************"<<endl;
            }

            else if(wybor=='h')
            {
                cout<<endl<<"Samolot zostal dodany przez uzytkownika"<<endl;
                lotnisko->NadlatujacySamolot(atof(rozpietosc),typ,atoi(l_pasazerow));
                licznikk++;
                cout<<endl<<"**************************"<<endl;
            }
                }while(licznikk==0);
                Sleep(4000);
        }



    }

}

bool CSymulacja::JestLiczba(char* string)
{
    int dlugosc=strlen(string);
    int licznik=0;
    for(int i=0;i<dlugosc;i++)
        {
    if(string[i]!='0'&&string[i]!='1'&&string[i]!='2'&&string[i]!='3'&&string[i]!='4'&&string[i]!='5'&&string[i]!='6'&&string[i]!='7'&&string[i]!='8'&&string[i]!='9'&&string[i]!=','&&string[i]!='.'&&string[i]!=32)
    {
        return false;
    }
    else licznik++;
        }
        if (licznik==dlugosc) return true;
}
