#include "kontroler.h"
#include <cstdlib>
#include <ctime>
#include "time.h"

using namespace std;


CKontroler::CKontroler()
{
    srand(time(NULL));
    widocznosc=50;
    stanPasa=0;
}

CKontroler::~CKontroler()
{

}

int CKontroler::SprawdzWidocznosc()
{
    if(licznik%5==0) widocznosclos=rand()%100+1;
    if(widocznosclos>widocznosc)
    {
        widocznosc=widocznosc+5;
    }
    else if(widocznosclos<widocznosc)
    {
        widocznosc=widocznosc-5;
    }
    licznik++;
    Informacja();
    if(widocznosc>30)
    {
        cout<<"Widocznosc pozwala na manewry"<<endl;
        return 0;
    }
    else
    {
        cout<<"Widocznosc nie pozwala na manewry"<<endl;
        return 1;
    }

}

void CKontroler::Informacja()
{
    cout<<"Pas startowy jest ";
    if(stanPasa==0) cout<<"pusty, ";
    if(stanPasa==1) cout<<"zajety, ";
    cout<<"widocznosc wynosi: "<<widocznosc<<"%"<<endl;
}

int CKontroler::SprawdzPas()
{

    if(stanPasa==0)
    {
        return 0;
    }
    else if(stanPasa==1)
    {

        return 1;
    }
}

void CKontroler::ustawPas(int stan)
{
    stanPasa=stan;
}
