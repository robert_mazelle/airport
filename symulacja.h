#include <iostream>
#include "lotnisko.h"
#pragma once

using namespace std;

class CSymulacja
{
    CLotnisko* lotnisko;
    int licznik,licznik2;
    void Sprawdz();
    bool JestLiczba(char* string);

public:
    CSymulacja(CLotnisko*);
    ~CSymulacja();
    void Symuluj();

};
