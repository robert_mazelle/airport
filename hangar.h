#pragma once
#include <iostream>
#include "samolot.h"

class CHangar
{
    int l_miejsc,miejsce[100];
    int SprawdzStan(CSamolot* Sprawdzany);

public:
    CHangar(int LiczbaMiejsc);
    ~CHangar();
    void WprowadzDoHangaru(CSamolot* Wprowadzany);
    int SprawdzMiejsca();
    int WyprowadzZHangaru(CSamolot* Wyprowadzany);
    void Informacja();
};
