#include "samolot.h"
#include <cstdlib>
#include <ctime>
#include "time.h"

using namespace std;

char* CSamolot::typy[10]={"Cessna 172","Embraer ERJ145 LR","McDonnell Douglas DC-9","Airbus A320neo","Airbus A320-200","Tu-154","Boeing 747-900ER","Ilyushin Il-400","Airbus A380","Boeing 777-200"};

CSamolot::CSamolot(float roz,char* tp,int pasazer)
{
    srand(time(NULL));
    rozpietosc=roz;
    typ=tp;
    l_pasazerow=pasazer;
    myStan=0;
    wiek=rand()%20;
}

CSamolot::CSamolot()
{
    int losuj;
    losuj=rand()%10;
    typ=typy[losuj];

    switch(losuj)
    {
    case 0:
        {
            rozpietosc=11;
            l_pasazerow=0;
            break;
        }
    case 1:
        {
            rozpietosc=20.04;
            l_pasazerow=50;
            break;
        }
    case 2:
        {
            rozpietosc=24.4;
            l_pasazerow=106;
            break;
        }
    case 3:
        {
            rozpietosc=33.91;
            l_pasazerow=189;
            break;
        }
    case 4:
        {
            rozpietosc=33.91;
            l_pasazerow=180;
            break;
        }
    case 5:
        {
            rozpietosc=37.55;
            l_pasazerow=180;
            break;
        }
    case 6:
        {
            rozpietosc=34.22;
            l_pasazerow=215;
            break;
        }
    case 7:
        {
            rozpietosc=60.11;
            l_pasazerow=436;
            break;
        }
    case 8:
        {
            rozpietosc=79,80;
            l_pasazerow=853;
            break;
        }
    case 9:
        {
            rozpietosc=60.9;
            l_pasazerow=301;
            break;
        }
    }
    wiek=rand()%20;
}

CSamolot::~CSamolot()
{

}

void CSamolot::Garazuj()
{
    DajStan(0);
}

void CSamolot::Laduj()
{
    DajStan(1);
}

void CSamolot::Startuj()
{
    DajStan(2);
}

void CSamolot::CzekajNaStart()
{
    DajStan(3);
}

void CSamolot::Nadlatujacy()
{
    DajStan(4);
}

void CSamolot::Nowy()
{
    DajStan(5);
    wiek=0;
}

void CSamolot::DajStan(int stan)
{
    myStan=stan;
}


void CSamolot::InformacjaS()
{

    cout<<"Samolot "<<typ<<", rok wybudowania: "<<2016-wiek<<"r ";

    switch(myStan)
    {
        case 0:
            {
                cout<<"jest garazowany, miejsce: "<<miejsce<<endl;
                break;
            }
        case 1:
            {
                cout<<"laduje, liczba pasazerow na pokladzie wynosi: "<<pasazerowie<<endl;
                 break;
            }
        case 2:
            {
                cout<<"startuje, liczba pasazerow na pokladzie wynosi: "<<pasazerowie<<endl;
                break;
            }
        case 3:
            {
                cout<<"czeka na mozliwosc startu"<<endl;
                break;
            }
        case 4:
            {
                cout<<"pojawia sie na horyzoncie"<<endl;
                break;
            }
        case 5:
            {
                cout<<"zostal dodany do floty lotniska"<<endl;
                break;
            }
            break;
    }
}



void CSamolot::DodajPasazerow()
{
        if(l_pasazerow>0)
    {
    int losuj2=l_pasazerow/2;
    pasazerowie=rand()%(losuj2-1)+(losuj2+1);
    }
    else pasazerowie=0;
}

void CSamolot::DajMiejsce(int Miejsce)
{
    miejsce=Miejsce;
}

char CSamolot::ZwrocHangar()
{
    return hangar;
}

int CSamolot::ZwrocMiejsce()
{
    return miejsce;
}

void CSamolot::DajHangar(char Hangar)
{
    hangar=Hangar;
}

int CSamolot::ZwrocWiek()
{
    return wiek;
}

int CSamolot::ZwrocLPasazerow()
{
    return l_pasazerow;
}

int CSamolot::ZwrocRozpietosc()
{
    return rozpietosc;
}

void CSamolot::DajPasazerow(int Pasazerowie)
{
    pasazerowie=Pasazerowie;
}
