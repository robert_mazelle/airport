#pragma once
#include <iostream>
#define CRT_SECURE_ NO_WARNINGS

class CSamolot
{
    int myStan,miejsce,pasazerowie,l_pasazerow,wiek;
    char hangar;
    float rozpietosc;
    char* typ;
    static char* typy[10];
    void DajStan(int Stan);

public:
    CSamolot(float Rozpietosc,char* Typ,int LiczPasazerow);
    CSamolot();
    ~CSamolot();
    void Garazuj();
    void Laduj();
    void Startuj();
    void CzekajNaStart();
    void Nadlatujacy();
    void Nowy();
    void DodajPasazerow();
    void InformacjaS();
    void DajPasazerow(int IlePasazerow);
    void DajMiejsce(int miejsce);
    char ZwrocHangar();
    int ZwrocMiejsce();
    void DajHangar(char Hangar);
    int ZwrocWiek();
    int ZwrocLPasazerow();
    int ZwrocRozpietosc();

};

