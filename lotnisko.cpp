#include "lotnisko.h"
#include <ctime>
#include <cstdlib>
#include "time.h"
#include "conio.h"
#include <windows.h>

using namespace std;

CLotnisko::CLotnisko(CKontroler *kontrol)
{
    srand(time(NULL));
    pasazerowie=0;
    hangar_A=new CHangar(60);
    hangar_B=new CHangar(30);
    hangar_C=new CHangar(10);
    for(int j=0;j<10;j++)
    {
        garazowane[j]=new CSamolot;
        garazowane[j]->Garazuj();
        Garazuj(garazowane[j]);
        ladujace[j]=NULL;
        czekajace[j]=NULL;
    }
    cout<<endl<<endl<<"//////////////////////////"<<endl<<endl;
    for(int i=10;i<100;i++)
    {
        garazowane[i]=NULL;
        ladujace[i]=NULL;
        czekajace[i]=NULL;
    }
    andrzej=kontrol;


}

CLotnisko::~CLotnisko()
{
    for(int i=0;i<100;i++)
    {
        if(garazowane[i]!=NULL) delete garazowane[i];
        if(ladujace[i]!=NULL) delete ladujace[i];
        if(czekajace[i]!=NULL) delete czekajace[i];
    }
    delete hangar_A;
    delete hangar_B;
    delete hangar_C;

}



void CLotnisko::NadlatujacySamolot()
{
    for(int i=0;i<100;i++)
    {
        if (ladujace[i]==NULL)
        {
            ladujace[i]=new CSamolot;
            ladujace[i]->Nadlatujacy();
            ladujace[i]->DodajPasazerow();
            ladujace[i]->InformacjaS();
            break;
        }
    }
}

void CLotnisko::NadlatujacySamolot(float roz,char* tp,int pasazer)
{
    for(int i=0;i<100;i++)
    {
        if (ladujace[i]==NULL)
        {
            ladujace[i]=new CSamolot(roz,tp,pasazer);
            ladujace[i]->Nadlatujacy();
            ladujace[i]->DodajPasazerow();
            ladujace[i]->InformacjaS();
            break;
        }
    }
}

void CLotnisko::NowySamolot()
{
    for(int i=0;i<100;i++)
    {
        if (garazowane[i]==NULL)
        {
            garazowane[i]=new CSamolot;
            garazowane[i]->Nowy();
            garazowane[i]->InformacjaS();
            garazowane[i]->Garazuj();
            Garazuj(garazowane[i]);
            break;
        }
    }

}

void CLotnisko::NowySamolot(float roz,char* tp, int pasazer)
{
    for(int i=0;i<100;i++)
    {
        if (garazowane[i]==NULL)
        {
            garazowane[i]=new CSamolot(roz,tp,pasazer);
            garazowane[i]->Nowy();
            garazowane[i]->InformacjaS();
            garazowane[i]->Garazuj();
            Garazuj(garazowane[i]);
            break;
        }
    }

}

void CLotnisko::Ladowanie()
{
   CHangar* hangar;
     for(int i=0;i<100;i++)
    {
        if(ladujace[i]!=NULL)
        {
            if(ladujace[i]->ZwrocHangar()=='A') hangar=hangar_A;
            else if(ladujace[i]->ZwrocHangar()=='B') hangar=hangar_B;
            else hangar=hangar_C;
            if(andrzej->SprawdzPas()==0)
            {
                if(andrzej->SprawdzWidocznosc()==0)
                {

                    if(hangar->SprawdzMiejsca()>0)
                    {
            ladujace[i]->Laduj();
            andrzej->ustawPas(1);
            ladujace[i]->InformacjaS();
            for(int l=0;l<100;l++)
            {
                if(garazowane[l]==NULL)
                {
                    garazowane[l]=ladujace[i];
                    Garazuj(garazowane[l]);
                    break;
                }
            }
            for(int j=i;j<100-i;j++)
            {
                if(ladujace[j+1]!=NULL)
                {
                    ladujace[j]=ladujace[i+1];
                }
                else
                {
                    ladujace[j]=NULL;
                    break;
                }
            }
        }
        else{
            cout<<"Brak miejsca na lotnisku dla samolotu o takim rozmiarze"<<endl;
            PrzekierujNaInneLotnisko(ladujace[i]);
        }
                }
        else
        {
            PrzekierujNaInneLotnisko(ladujace[i]);
            break;
        }
            }
            break;
        }

    }
    cout<<endl<<"**************************"<<endl;

}

void CLotnisko::Startowanie()
{
    for(int i=0;i<100;i++)
    {

        if(czekajace[i]!=NULL)
        {

            if(andrzej->SprawdzPas()==0)
            {
                if(andrzej->SprawdzWidocznosc()==0)
                {
                    cout<<endl<<"3"<<endl;
                    Sleep(1000);
                    cout<<"2"<<endl;
                    Sleep(1000);
                    cout<<"1"<<endl<<endl;
                    Sleep(1000);
                    if(czekajace[i]->ZwrocLPasazerow()>0) DajPasazerow(czekajace[i]);
                    czekajace[i]->Startuj();
                    andrzej->ustawPas(1);
                    czekajace[i]->InformacjaS();
            for(int j=i;j<100-i;j++)
            {
                if(czekajace[j+1]!=NULL)
                {
                    czekajace[j]=czekajace[i+1];
                }
                else
                {
                    czekajace[j]=0;
                    break;
                }

        }
                }
            }
            break;

        }
    }
    cout<<endl<<"**************************"<<endl;
}

void CLotnisko::PrzekierujNaInneLotnisko(CSamolot* samolot)
{
    cout<<"Samolot zostaje przekierowany na inne lotnisko"<<endl;
}

void CLotnisko::NowyCykl()
{
    andrzej->ustawPas(0);
}

void CLotnisko::LosujStartujacy(int pula)
{
    int los;
    CSamolot* wylosowany;
    los=rand()%pula;
    wylosowany=garazowane[los];
    wylosowany->InformacjaS();
    if(Wyprowadz(wylosowany)==0)
       {
    for(int j=los;j<100-los;j++)
    {
        if(garazowane[j+1]!=NULL) garazowane[j]=garazowane[j+1];
        else
        {
            garazowane[j]=NULL;
            break;
        }
    }
    for (int k=0;k<100;k++)
    {
        if(czekajace[k]==NULL) czekajace[k]=wylosowany;
        break;
    }
       }
}


int CLotnisko::Zlicz(char ktore)
{
    int temp=0;
    if(ktore=='l')
    {
        for(int i=0;i<100;i++)
           {
                if(ladujace[i]!=NULL) temp++;
           }

    }

    else if(ktore=='g')
    {
         for(int i=0;i<100;i++)
           {
                if(garazowane[i]!=NULL) temp++;
           }
    }

    return temp;
}

void CLotnisko::Garazuj(CSamolot* garazuj)
{
    if(garazuj->ZwrocRozpietosc()<=34)
    {
        garazuj->DajHangar('A');
        hangar_A->WprowadzDoHangaru(garazuj);
    }
    else if(garazuj->ZwrocRozpietosc()<79)
    {
        garazuj->DajHangar('B');
        hangar_B->WprowadzDoHangaru(garazuj);

    }
    else if(garazuj->ZwrocRozpietosc()>=79)
    {
        garazuj->DajHangar('C');
        hangar_C->WprowadzDoHangaru(garazuj);

    }
    garazuj->Garazuj();
}

int CLotnisko::Wyprowadz(CSamolot* wyprowadz)
{
    if(wyprowadz->ZwrocHangar()=='A')
        {
            if(hangar_A->WyprowadzZHangaru(wyprowadz)==0) return 0;
            else return 1;
        }
    else if(wyprowadz->ZwrocHangar()=='B')
    {
        if(hangar_B->WyprowadzZHangaru(wyprowadz)==0) return 0;
        else return 1;
    }
    else
    {
        if(hangar_C->WyprowadzZHangaru(wyprowadz)==0)return 0;
        else return 1;
        }
    wyprowadz->CzekajNaStart();
}

void CLotnisko::NowiPasazerowie()
{
    pasazerowie=pasazerowie+rand()%150;
}

void CLotnisko::DajPasazerow(CSamolot* samolot)
{
    int losuj=rand()%(samolot->ZwrocLPasazerow()/2)+(samolot->ZwrocLPasazerow()/2);
    if (pasazerowie<losuj) int losuj=rand()%pasazerowie/2+pasazerowie/2;
    samolot->DajPasazerow(losuj);
}
